# 3x3 matrix Cramer Determinant calculator
# @author: Rafael Oliveira
     # keys: 0    1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   19   20   21   22   23   24   25   26   27   28   29   30
alphabet = ['?', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '.', '!', '#', ' ']

puts "Cryptomatrix - by Rafael Oliveira"
puts "e - Encode        | d - Decode"
option = gets.chomp.downcase

if option == "e"
	puts "you choosed encode"
	puts "Enter message to encode"
	message = gets.chomp.upcase.chars.to_a
	if message.length % 2 != 0
		message.insert(-1," ")
	end	
	

	# create array for first line
	a = Array.new
	for i in 0...message.length / 2
		a.insert(i,message[i])
	end

	# create array for second line
	b = Array.new
	pos = 0  #array position controller
	for i in message.length / 2...message.length
		b.insert(pos,message[i])
		pos += 1
	end

	puts "Message to encode:"
	puts "#{a}"
	puts "#{b}"	

	#translate the message to numbers, then encode with the key matrix
	a_numbers = Array.new
	b_numbers = Array.new

	# you can use a.length for both because both arrays will have the same size ;)
	for i in 0...a.length
		a_numbers.insert(i,alphabet.index(a[i]))
		b_numbers.insert(i,alphabet.index(b[i]))
	end

	puts "Message in Number table:"
	puts "#{a_numbers}"
	puts "#{b_numbers}"

	puts "Enter values of the key matrix to ENCODE, separated by commas:"
	key_matrix = gets.chomp.split(",")

	#encode the number matrix using the key matrix
	a_encoded = Array.new
	b_encoded = Array.new

	for i in 0...a.length
		a_encoded.insert(i,((Integer(key_matrix[0]) * Integer(a_numbers[i])) + ((Integer(key_matrix[1]) * Integer(b_numbers[i])))))
		b_encoded.insert(i,((Integer(key_matrix[2]) * Integer(a_numbers[i])) + ((Integer(key_matrix[3]) * Integer(b_numbers[i])))))
	end


	puts "Encoded message:"
	puts "#{a_encoded}"
	puts "#{b_encoded}"

elsif option == "d"
	puts "you choosed decode"
	puts "Enter encoded message values separated by commas:"
	encoded_matrix = gets.chomp.split(",")

	# create array for first line
	a_encoded = Array.new
	for i in 0...encoded_matrix.length / 2
		a_encoded.insert(i,encoded_matrix[i])
	end

	# create array for second line
	b_encoded = Array.new
	pos = 0  #array position controller
	for i in encoded_matrix.length / 2...encoded_matrix.length
		b_encoded.insert(pos,encoded_matrix[i])
		pos += 1
	end

	puts "Enter values of the key matrix to DECODE, separated by commas:"
	key_matrix = gets.chomp.split(",")

	#create arrays for message in number table

	a_numbers = Array.new
	b_numbers = Array.new

	for i in 0...a_encoded.length
		a_numbers.insert(i,((Integer(key_matrix[0]) * Integer(a_encoded[i])) + ((Integer(key_matrix[1]) * Integer(b_encoded[i])))))
		b_numbers.insert(i,((Integer(key_matrix[2]) * Integer(a_encoded[i])) + ((Integer(key_matrix[3]) * Integer(b_encoded[i])))))
	end

	puts "Message in Number table:"
	puts "#{a_numbers}"
	puts "#{b_numbers}"

	
	#create arrays for decoded message

	a = Array.new
	b = Array.new

	for i in 0...a_numbers.length
		a.insert(i,alphabet[a_numbers[i]])
		b.insert(i,alphabet[b_numbers[i]])
	end

	puts "Decoded message:"
	puts "#{a}"
	puts "#{b}"

else
	puts "Invalid option, exiting..."
end
