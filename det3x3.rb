# 3x3 matrix Determinant calculator
# @author: Rafael Oliveira

puts "enter 3x3 matrix values separated by comma"
puts "(values 1 to 9)"
m = gets.chomp.split(",")
part1 = (Integer(m[0])*Integer(m[4])*Integer(m[8]))+(Integer(m[1])*Integer(m[5])*Integer(m[6]))+(Integer(m[2])*Integer(m[3])*Integer(m[7]))
part2 = (Integer(m[1])*Integer(m[3])*Integer(m[8]))+(Integer(m[0])*Integer(m[5])*Integer(m[7]))+(Integer(m[2])*Integer(m[4])*Integer(m[6]))
det = part1 - part2

puts "part1: #{part1}"
puts "part2: #{part2}"
puts "Determinant is: #{det}"
 
