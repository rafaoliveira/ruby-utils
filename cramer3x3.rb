# 3x3 matrix Cramer Determinant calculator
# @author: Rafael Oliveira

puts "enter values for X, Y, Z, and result for the first equation, separated by comma: "
a = gets.chomp.split(",")

puts "enter values for X, Y, Z, and result for the second equation, separated by comma: "
b = gets.chomp.split(",")

puts "enter values for X, Y, Z, and result for the third equation, separated by comma: "
c = gets.chomp.split(",")


#find main determinant
part1 = (Integer(a[0])*Integer(b[1])*Integer(c[2]))+(Integer(a[1])*Integer(b[2])*Integer(c[0]))+(Integer(a[2])*Integer(b[0])*Integer(c[1]))
part2 = (Integer(a[1])*Integer(b[0])*Integer(c[2]))+(Integer(a[0])*Integer(b[2])*Integer(c[1]))+(Integer(a[2])*Integer(b[1])*Integer(c[0]))
det = part1 - part2

#find determinant for X
part1 = (Integer(a[3])*Integer(b[1])*Integer(c[2]))+(Integer(a[1])*Integer(b[2])*Integer(c[3]))+(Integer(a[2])*Integer(b[3])*Integer(c[1]))
part2 = (Integer(a[1])*Integer(b[3])*Integer(c[2]))+(Integer(a[3])*Integer(b[2])*Integer(c[1]))+(Integer(a[2])*Integer(b[1])*Integer(c[3]))
detX = part1 - part2

#find determinant for Y
part1 = (Integer(a[0])*Integer(b[3])*Integer(c[2]))+(Integer(a[3])*Integer(b[2])*Integer(c[0]))+(Integer(a[2])*Integer(b[0])*Integer(c[3]))
part2 = (Integer(a[3])*Integer(b[0])*Integer(c[2]))+(Integer(a[0])*Integer(b[2])*Integer(c[3]))+(Integer(a[2])*Integer(b[3])*Integer(c[0]))
detY = part1 - part2

#find determinant for Z
part1 = (Integer(a[0])*Integer(b[1])*Integer(c[3]))+(Integer(a[1])*Integer(b[3])*Integer(c[0]))+(Integer(a[3])*Integer(b[0])*Integer(c[1]))
part2 = (Integer(a[1])*Integer(b[0])*Integer(c[3]))+(Integer(a[0])*Integer(b[3])*Integer(c[1]))+(Integer(a[3])*Integer(b[1])*Integer(c[0]))
detZ = part1 - part2



puts "Det: #{det}"
puts "Det X: #{detX}"
puts "Det Y: #{detY}"
puts "Det Z: #{detZ}"
 