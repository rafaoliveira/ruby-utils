# 2x2 matrix Cramer Determinant calculator
# @author: Rafael Oliveira

puts "enter values for X, Y, and result for the first equation, separated by comma: "
a = gets.chomp.split(",")

puts "enter values for X, Y, and result for the second equation, separated by comma: "
b = gets.chomp.split(",")

det = (Integer(a[0])*Integer(b[1])) - (Integer(a[1])*Integer(b[0]))
detX = (Integer(a[2])*Integer(b[1])) - (Integer(a[1])*Integer(b[2]))
detY = (Integer(a[0])*Integer(b[2])) - (Integer(a[2])*Integer(b[0]))

puts "Det: #{det}"
puts "Det X: #{detX}"
puts "Det Y: #{detY}"
 