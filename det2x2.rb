# 2x2 matrix Determinant calculator
# @author: Rafael Oliveira

print "enter 2x2 matrix values separated by comma: "
    m = gets.chomp.split(",")
    det = Integer(m[0])*Integer(m[3]) - Integer(m[1])*Integer(m[2])
    puts "Determinant is: #{det}"
 